<?php


namespace Fxscripts\ProgramActions;


use Fxscripts\Entities\RateEntity;
use Fxscripts\Entities\AlertEntity;
use Fxscripts\Helpers\ItemFormat;
use Fxscripts\Services\MailerService;

class MarketEnterProgramAction extends ProgramAction
{
	public function doProgram(RateEntity $currentRate, RateEntity $previousRate)
	{
		if($currentRate->getClosePrice() >= $previousRate->getHighPrice()) {
			if($currentRate->getLowPrice() >= $previousRate->getLowPrice()) {
				$alert = new AlertEntity();
				$alert->setSubject('Программа N1. Цена выросла. Вход на рынок');
				$body = 'Программа N1. Цена выросла. Вход на рынок' . '<br>';
				$body .= "Последняя свеча({$currentRate->getDateOpenPrice()} - {$currentRate->getDateClosePrice()}):" .
				         ' Close: ' . ItemFormat::priceFormat($currentRate->getClosePrice()) .
				         ' Low: ' . ItemFormat::priceFormat($currentRate->getLowPrice()) .
				         ' High ' . ItemFormat::priceFormat($currentRate->getHighPrice());
				$body .= '<br>';
				$body .= "Предыдущая свеча({$previousRate->getDateOpenPrice()} - {$previousRate->getDateClosePrice()}):" .
				         ' Close: ' . ItemFormat::priceFormat($previousRate->getClosePrice()) .
				         ' Low: ' . ItemFormat::priceFormat($previousRate->getLowPrice()) .
				         ' High ' . ItemFormat::priceFormat($previousRate->getHighPrice());

				$alert->setBody($body);
				$this->sendAlert($alert);
			}
		}

		if($currentRate->getClosePrice() <= $previousRate->getLowPrice()) {
			if($currentRate->getHighPrice() <= $previousRate->getHighPrice()) {

				$alert = new AlertEntity();
				$alert->setSubject('Программа N1. Цена снизилась. Вход на рынок');
				$body = 'Программа N1. Цена снизилась. Вход на рынок' . '<br>';
				$body .= "Последняя свеча({$currentRate->getDateOpenPrice()} - {$currentRate->getDateClosePrice()}):" .
				         ' Close: ' . ItemFormat::priceFormat($currentRate->getClosePrice()) .
				         ' Low: ' . ItemFormat::priceFormat($currentRate->getLowPrice()) .
				         ' High ' . ItemFormat::priceFormat($currentRate->getHighPrice());
				$body .= '<br>';
				$body .= "Предыдущая свеча({$previousRate->getDateOpenPrice()} - {$previousRate->getDateClosePrice()}):" .
				         ' Close: ' . ItemFormat::priceFormat($previousRate->getClosePrice()) .
				         ' Low: ' . ItemFormat::priceFormat($previousRate->getLowPrice()) .
				         ' High ' . ItemFormat::priceFormat($previousRate->getHighPrice());

				$alert->setBody($body);
				$this->sendAlert($alert);
			}
		}
	}

	private function sendAlert(AlertEntity $alert)
	{
		$mailer = new MailerService();
		$alert->setAddresses([SITE_MAIL]);

		$mailer->sendEmail($alert);
	}

    private function sendTestAlert($currentRate, $previousRate)
    {
        $alert = new AlertEntity();
        $alert->setSubject('Программа N1. Лог');
        $body = "Последняя свеча({$currentRate->getDateClosePrice()}):" .
                 ' Close: ' . ItemFormat::priceFormat($currentRate->getClosePrice()) .
                 ' Low: ' . ItemFormat::priceFormat($currentRate->getLowPrice()) .
                 ' High ' . ItemFormat::priceFormat($currentRate->getHighPrice());
        $body .= '<br>';
        $body .= "Предыдущая свеча({$previousRate->getDateClosePrice()}):" .
                 ' Close: ' . ItemFormat::priceFormat($previousRate->getClosePrice()) .
                 ' Low: ' . ItemFormat::priceFormat($previousRate->getLowPrice()) .
                 ' High ' . ItemFormat::priceFormat($previousRate->getHighPrice());

        $alert->setBody($body);

        $mailer = new MailerService();
        $alert->setAddresses(['mkrios42@gmail.com']);

        $mailer->sendEmail($alert);
    }
}