<?php


namespace Fxscripts\ProgramActions;


use Fxscripts\Entities\AlertEntity;
use Fxscripts\Entities\ProgramEntity;
use Fxscripts\Entities\RateEntity;
use Fxscripts\Helpers\ItemFormat;
use Fxscripts\Services\MailerService;

class LimitProgramAction extends ProgramAction
{

	const RATE_LIMIT = 0.0018;

	public function doProgram(ProgramEntity $programEntity, RateEntity $currentRate)
	{
		$settings = $programEntity->getSettingsArray();
		$pointRate = $settings['rate_point'];
		$pointDate = new \DateTime($settings['time_point']);
		$deltaRate = $currentRate->getCurrentPrice()-floatval($pointRate);
		if(abs($deltaRate) >= self::RATE_LIMIT) {
			$direction = $deltaRate > 0 ? 'вверх' : 'вниз';
			$alert = new AlertEntity();
			$alert->setSubject('Программа N2. Цена изменилась с точки отсчета');
			$alert->setBody(
				"Программа N2. Цена изменилась с точки отсчета " . ItemFormat::priceFormat($pointRate) . " c {$pointDate->format('d.m.Y H:i')} " .
				" {$direction} и составляет сейчас " . ItemFormat::priceFormat($currentRate->getCurrentPrice()));

			$this->sendAlert($alert);
			return true;
		}

		return false;
	}

	public function sendAlert(AlertEntity $alert)
	{
		$mailer = new MailerService();

		$alert->setAddresses([SITE_MAIL]);
		$mailer->sendEmail($alert);
	}
}