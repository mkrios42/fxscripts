<?php
/**
 * Created by PhpStorm.
 * User: mkislitsyn
 * Date: 19.03.2019
 * Time: 16:39
 */

namespace Fxscripts\Entities;

class ExtractorRequest
{
	/** @var string */
	private $baseUri;
	/** @var string */
	private $endpoint;
	/** @var string */
	private $httpMethod;
	/** @var array */
	private $requestParams;
	/** @var array */
	private $headers = [];
	/** @var int */
	private $timeout = 30;

	/**
	 * @return string
	 */
	public function getBaseUri()
	{
		return $this->baseUri;
	}

	/**
	 * @param string $baseUri
	 */
	public function setBaseUri($baseUri)
	{
		$this->baseUri = $baseUri;
	}

	/**
	 * @return string
	 */
	public function getEndpoint()
	{
		return $this->endpoint;
	}

	/**
	 * @param string $endpoint
	 */
	public function setEndpoint($endpoint)
	{
		$this->endpoint = $endpoint;
	}

	/**
	 * @return string
	 */
	public function getHttpMethod()
	{
		return $this->httpMethod;
	}

	/**
	 * @param string $httpMethod
	 */
	public function setHttpMethod($httpMethod)
	{
		$this->httpMethod = $httpMethod;
	}

	/**
	 * @return array
	 */
	public function getRequestParams()
	{
        return $this->requestParams;
	}

	/**
	 * @param array $requestParams
	 */
	public function setRequestParams($requestParams)
	{
		$this->requestParams = $requestParams;
	}

	public function addHeader($key, $value)
    {
        $this->headers[$key] = $value;
    }

	/**
	 * @return array
	 */
	public function getHeaders()
	{
		return $this->headers;
	}

	/**
	 * @param array $headers
	 */
	public function setHeaders($headers)
	{
		$this->headers = $headers;
	}

	/**
	 * @return int
	 */
	public function getTimeout()
	{
		return $this->timeout;
	}

	/**
	 * @param int $timeout
	 */
	public function setTimeout($timeout)
	{
		$this->timeout = $timeout;
	}
}