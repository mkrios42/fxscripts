<?php


namespace Fxscripts\Entities;


class ProgramEntity
{
	/** @var int */
	private $id;
	/** @var string */
	private $gid;
	/** @var string */
	private $name;
	/** @var bool */
	private $status;
	/** @var string */
	private $settings;
	/** @var string */
	private $namespace;

	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId(int $id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getGid(): string
	{
		return $this->gid;
	}

	/**
	 * @param string $gid
	 */
	public function setGid(string $gid)
	{
		$this->gid = $gid;
	}

	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name)
	{
		$this->name = $name;
	}

	/**
	 * @return bool
	 */
	public function getStatus(): bool
	{
		return $this->status;
	}

	/**
	 * @param bool $status
	 */
	public function setStatus(bool $status)
	{
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getSettingsArray()
	{
		return json_decode($this->settings, true);
	}

	/**
	 * @param array $settings
	 */
	public function setSettingsArray($settings = [])
	{
		$this->settings = json_encode($settings);
	}

	/**
	 * @return string
	 */
	public function getSettings(): string
	{
		return $this->settings;
	}

	/**
	 * @param string $settings
	 */
	public function setSettings(string $settings)
	{
		$this->settings = $settings;
	}

	/**
	 * @return string
	 */
	public function getNamespace(): string
	{
		return $this->namespace;
	}

	/**
	 * @param string $namespace
	 */
	public function setNamespace(string $namespace)
	{
		$this->namespace = $namespace;
	}
}