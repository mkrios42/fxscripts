<?php


namespace Fxscripts\Entities;


class AlertEntity
{
	/** @var array */
	private $addresses;
	/** @var string */
	private $subject;
	/** @var string */
	private $body;

	/**
	 * @return array
	 */
	public function getAddresses(): array
	{
		return $this->addresses;
	}

	/**
	 * @param array $addresses
	 */
	public function setAddresses(array $addresses)
	{
		$this->addresses = $addresses;
	}

	/**
	 * @return string
	 */
	public function getSubject(): string
	{
		return $this->subject;
	}

	/**
	 * @param string $subject
	 */
	public function setSubject(string $subject)
	{
		$this->subject = $subject;
	}

	/**
	 * @return string
	 */
	public function getBody(): string
	{
		return $this->body;
	}

	/**
	 * @param string $body
	 */
	public function setBody(string $body)
	{
		$this->body = $body;
	}
}