<?php


namespace Fxscripts\Entities;


class RateEntity
{
	/** @var string */
	private $pair;
	/** @var float */
	private $currentPrice;
	/** @var float */
	private $askPrice;
	/** @var float */
	private $bidPrice;
	/** @var float */
	private $openPrice;
	/** @var float */
	private $closePrice;
	/** @var float */
	private $lowPrice;
	/** @var float */
	private $highPrice;
	/** @var string */
	private $dateOpenPrice;
	/** @var string */
	private $dateClosePrice;
	/** @var string */
	private $datePrice;

	/**
	 * @return string
	 */
	public function getPair(): string
	{
		return $this->pair;
	}

	/**
	 * @param string $pair
	 */
	public function setPair(string $pair)
	{
		$this->pair = $pair;
	}

	/**
	 * @return float
	 */
	public function getCurrentPrice(): float
	{
		return $this->currentPrice;
	}

	/**
	 * @param float $currentPrice
	 */
	public function setCurrentPrice(float $currentPrice)
	{
		$this->currentPrice = $currentPrice;
	}

	/**
	 * @return float
	 */
	public function getAskPrice(): float
	{
		return $this->askPrice;
	}

	/**
	 * @param float $askPrice
	 */
	public function setAskPrice(float $askPrice)
	{
		$this->askPrice = $askPrice;
	}

	/**
	 * @return float
	 */
	public function getBidPrice(): float
	{
		return $this->bidPrice;
	}

	/**
	 * @param float $bidPrice
	 */
	public function setBidPrice(float $bidPrice)
	{
		$this->bidPrice = $bidPrice;
	}

	/**
	 * @return float
	 */
	public function getOpenPrice(): float
	{
		return $this->openPrice;
	}

	/**
	 * @param float $openPrice
	 */
	public function setOpenPrice(float $openPrice)
	{
		$this->openPrice = $openPrice;
	}

	/**
	 * @return float
	 */
	public function getClosePrice(): float
	{
		return $this->closePrice;
	}

	/**
	 * @param float $closePrice
	 */
	public function setClosePrice(float $closePrice)
	{
		$this->closePrice = $closePrice;
	}

	/**
	 * @return float
	 */
	public function getLowPrice(): float
	{
		return $this->lowPrice;
	}

	/**
	 * @param float $lowPrice
	 */
	public function setLowPrice(float $lowPrice)
	{
		$this->lowPrice = $lowPrice;
	}

	/**
	 * @return float
	 */
	public function getHighPrice(): float
	{
		return $this->highPrice;
	}

	/**
	 * @param float $highPrice
	 */
	public function setHighPrice(float $highPrice)
	{
		$this->highPrice = $highPrice;
	}

	/**
	 * @return string
	 */
	public function getDateOpenPrice(): string
	{
		return $this->dateOpenPrice;
	}

	/**
	 * @param string $dateOpenPrice
	 */
	public function setDateOpenPrice(string $dateOpenPrice)
	{
		$this->dateOpenPrice = $dateOpenPrice;
	}

	/**
	 * @return string
	 */
	public function getDateClosePrice(): string
	{
		return $this->dateClosePrice;
	}

	/**
	 * @param string $dateClosePrice
	 */
	public function setDateClosePrice(string $dateClosePrice)
	{
		$this->dateClosePrice = $dateClosePrice;
	}

	/**
	 * @return string
	 */
	public function getDatePrice(): string
	{
		return $this->datePrice;
	}

	/**
	 * @param string $datePrice
	 */
	public function setDatePrice(string $datePrice)
	{
		$this->datePrice = $datePrice;
	}
}