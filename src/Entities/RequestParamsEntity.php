<?php


namespace Fxscripts\Entities;


class RequestParamsEntity
{
	/** @var string */
	private $pair;
	/** @var int */
	private $duration = 30;
	/** @var \DateTime */
	private $dateFrom;
	/** @var \DateTime */
	private $dateTo;

	const PAIR_GBPUSD = 'GBPUSD';

	/**
	 * @return string
	 */
	public function getPair(): string
	{
		return $this->pair;
	}

	/**
	 * @param string $pair
	 */
	public function setPair(string $pair)
	{
		$this->pair = $pair;
	}

	/**
	 * @return int
	 */
	public function getDuration(): int
	{
		return $this->duration;
	}

	/**
	 * @param int $duration
	 */
	public function setDuration(int $duration)
	{
		$this->duration = $duration;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateFrom(): \DateTime
	{
		return $this->dateFrom;
	}

	/**
	 * @param \DateTime $dateFrom
	 */
	public function setDateFrom(\DateTime $dateFrom)
	{
		$this->dateFrom = $dateFrom;
	}

	/**
	 * @return \DateTime
	 */
	public function getDateTo(): \DateTime
	{
		return $this->dateTo;
	}

	/**
	 * @param \DateTime $dateTo
	 */
	public function setDateTo(\DateTime $dateTo)
	{
		$this->dateTo = $dateTo;
	}
}