<?php


namespace Fxscripts\Helpers;


class ItemFormat
{
	public static function priceFormat($price)
	{
		return round($price, 5);
	}
}