<?php
/**
 * Created by PhpStorm.
 * User: mkislitsyn
 * Date: 19.03.2019
 * Time: 15:55
 */

namespace Fxscripts\Extractors;

use Fxscripts\Entities\ExtractorRequest;
use Fxscripts\Entities\ExtractorResponse;
use Fxscripts\Interfaces\ExtractorInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\GuzzleException;

class GuzzleExtractor implements ExtractorInterface
{
    private $authCounter = 0;

    private $tryCounter = 0;

    /**
     * @param ExtractorRequest $apiRequestParams
     *
     * @return ExtractorResponse
     * @throws \Exception
     */
	public function getUrl(ExtractorRequest $apiRequestParams)
	{
		$params = [];

		switch($apiRequestParams->getHttpMethod()) {
			case 'GET':
				$params = ['query' => $apiRequestParams->getRequestParams()];
				break;
			case 'POST':
				$params = ['form_params' => $apiRequestParams->getRequestParams()];
				break;
		}

		$client = new Client([
			'base_uri' => $apiRequestParams->getBaseUri(),
			'timeout'  => $apiRequestParams->getTimeout(),
		]);

		$request = new Request($apiRequestParams->getHttpMethod(), $apiRequestParams->getEndpoint(), $apiRequestParams->getHeaders());

		try {
			$response = $client->send($request, $params);

			if($response->getStatusCode() == 200) {
				$contents = $response->getBody()->getContents();
				return (new ExtractorResponse())->setResponse($contents);
			} else {
                if($this->tryCounter < 3) {
                    $this->tryCounter++;
                    sleep(60);
                    return $this->getUrl($apiRequestParams);
                } else {
                    throw new \Exception('[Investing API] Server error. Code: ' . $response->getStatusCode() . 'Reason: ' . $response->getReasonPhrase());
                }

			}
		} catch(GuzzleException $e) {
            if($this->tryCounter < 3) {
                $this->tryCounter++;
                sleep(60);
                return $this->getUrl($apiRequestParams);
            } else {
                throw new \Exception('[Investing API] Request error. Reason:' . $e->getMessage());
            }
		}
	}
}