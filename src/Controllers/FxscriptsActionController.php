<?php


namespace Fxscripts\Controllers;


use Fxscripts\Entities\RequestParamsEntity;
use Fxscripts\Providers\InvestingApiProvider;
use Fxscripts\Services\ProgramActionService;

class FxscriptsActionController
{
	/**
	 * @var \DB
	 */
	private $db;

	const TIME_INTERVAL = 30;

	public function __construct(\DB $db)
	{
		$this->db = $db;
	}

	public function doMarketEnterProgram()
	{
		$currentDate = new \DateTime();
		$minutes = $currentDate->format('i');
		$startMinutes = $this->formatMinutesToStr($this->getStartIntervalByMinutes($minutes));
		$dateTo = \DateTime::createFromFormat(DATETIME_FORMAT, $currentDate->format('Y-m-d H:' . $startMinutes . ':00'));
		$dateFrom = clone $dateTo;
		$dateFrom->modify('-30 minutes');

		$requestParams = new RequestParamsEntity();
		$requestParams->setPair(RequestParamsEntity::PAIR_GBPUSD);
		$requestParams->setDuration(self::TIME_INTERVAL);
		$requestParams->setDateFrom($dateFrom);
		$requestParams->setDateTo($dateTo);

		$programService = new ProgramActionService($this->db);
		$provider = new InvestingApiProvider();
		$programService->doMarketEnterProgram($requestParams, $provider);
	}

	public function doLimitPointProgram()
	{
		$requestParams = new RequestParamsEntity();
		$requestParams->setPair(RequestParamsEntity::PAIR_GBPUSD);
		$requestParams->setDateFrom((new \DateTime()));

		$programService = new ProgramActionService($this->db);
		$provider = new InvestingApiProvider();
		$programService->doLimitPointProgram($requestParams, $provider);
	}

	private function getStartIntervalByMinutes($minutes)
	{
		return (int)(floor($minutes/self::TIME_INTERVAL)*self::TIME_INTERVAL);
	}

	private function  formatMinutesToStr($minutes)
	{
		$minutes = strval($minutes);
		$minutes = (mb_strlen($minutes) == 1) ? '0' . $minutes : $minutes;

		return $minutes;
	}
}