<?php


namespace Fxscripts\Controllers;


use Fxscripts\Models\ProgramModel;
use Fxscripts\ProgramForms\LimitPointProgramForm;
use Fxscripts\ProgramForms\MarketEnterProgramForm;
use Fxscripts\Services\ProgramFormService;

class FxscriptsFormController
{
	/** @var \DB  */
	private $db;

	/** @var ProgramFormService */
	private $programService;

	public function __construct(\DB $db)
	{
		$this->db = $db;
	}

	public function getProgramForms()
	{
		echo $this->getProgramService()->getProgramForm(ProgramFormService::PROGRAM_MARKET_ENTER_GID);
		echo $this->getProgramService()->getProgramForm(ProgramFormService::PROGRAM_LIMIT_POINT_GID);
	}

	public function getProgramService()
	{
		if(!$this->programService) {
			$this->programService = new ProgramFormService($this->db);
		}

		return $this->programService;
	}
}