<?php


namespace Fxscripts\Services;


use Fxscripts\Entities\RequestParamsEntity;
use Fxscripts\Interfaces\ProviderInterface;
use Fxscripts\Models\ProgramModel;
use Fxscripts\ProgramActions\LimitProgramAction;
use Fxscripts\ProgramActions\MarketEnterProgramAction;

class ProgramActionService
{
	/**
	 * @var \DB
	 */
	private $db;

	/** @var ProgramModel */
	private $programModel;


	/**
	 * ProgramActionService constructor.
	 */
	public function __construct(\DB $db)
	{
		$this->db = $db;
	}

	public function doMarketEnterProgram(RequestParamsEntity $requestParams, ProviderInterface $provider)
	{
		$programEntity = $this->getProgramModel()->getProgramByGid(ProgramFormService::PROGRAM_MARKET_ENTER_GID);

		if($programEntity->getStatus()) {
			if($currentRate = $provider->getPreviousRate($requestParams)) {
				$previousRateParams = clone $requestParams;
				$previousRateParams->getDateFrom()->modify('-' . $previousRateParams->getDuration() . ' minutes');
				$previousRateParams->getDateTo()->modify('-' . $previousRateParams->getDuration() . ' minutes');

				$previousRate = $provider->getPreviousRate($previousRateParams);

				$programActionService = new MarketEnterProgramAction();
				$programActionService->doProgram($currentRate, $previousRate);
			}
		}
	}

	public function doLimitPointProgram(RequestParamsEntity $requestParams, ProviderInterface $provider)
	{
		$programEntity = $this->getProgramModel()->getProgramByGid(ProgramFormService::PROGRAM_LIMIT_POINT_GID);

		if($programEntity->getStatus()) {
			if($currentRate = $provider->getCurrentRate($requestParams)) {
				$programActionService = new LimitProgramAction();
				if($programActionService->doProgram($programEntity, $currentRate)) {
					$programEntity->setStatus(false);
					$this->getProgramModel()->updateProgram($programEntity);
				}
			}
		}

	}

	/**
	 * @return ProgramModel
	 */
	public function getProgramModel(): ProgramModel
	{
		if(!$this->programModel) {
			$this->programModel = new ProgramModel($this->db);
		}

		return $this->programModel;
	}
}