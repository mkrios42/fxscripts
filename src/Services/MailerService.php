<?php


namespace Fxscripts\Services;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Fxscripts\Entities\AlertEntity;

class MailerService
{
	/**
	 * @var PHPMailer|null
	 */
	private $mailer;

	public function sendEmail(AlertEntity $alert)
	{
		$mailer = $this->getMailer();

		foreach($alert->getAddresses() as $address) {
			$mailer->addAddress($address);
		}

		$mailer->Subject = $alert->getSubject();
		$mailer->Body    = $alert->getBody();

		try {
			$mailer->send();
		} catch(\Exception $e) {

		}
	}

	private function getMailer()
	{
		if(!$this->mailer) {
			$mail = new PHPMailer(true);
			$mail->isSMTP();
			$mail->Host = getenv('SMTP_HOST');
			$mail->SMTPAuth = true;
			$mail->Username = getenv('SMTP_USER');
			$mail->Password = getenv('SMTP_PASS');
			$mail->Port = 587;

			$mail->SMTPOptions = array(
				'ssl' => array(
					'verify_peer' => false,
					'verify_peer_name' => false,
					'allow_self_signed' => true
				)
			);

			$mail->setFrom('alerts@fxscripts.hhos.ru', 'FxScripts Alerts');
			$mail->isHTML(true);
			$mail->setLanguage('ru');
			$mail->CharSet = 'UTF-8';
			$mail->Encoding = 'base64';

			$this->mailer = $mail;
		}

		return $this->mailer;

	}
}