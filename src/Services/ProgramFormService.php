<?php


namespace Fxscripts\Services;


use Fxscripts\Models\ProgramModel;
use Fxscripts\ProgramForms\LimitPointProgramForm;
use Fxscripts\ProgramForms\MarketEnterProgramForm;

class ProgramFormService
{
	/**
	 * @var \DB
	 */
	private $db;

	/** @var ProgramModel */
	private $programModel;

	const PROGRAM_MARKET_ENTER_GID = 'market_enter_notify';
	const PROGRAM_LIMIT_POINT_GID = 'limit_point_notify';

	public function __construct(\DB $db)
	{
		$this->db = $db;
	}

	public function getProgramForm($gid)
	{
		$program = null;

		$entity = $this->getProgramModel()->getProgramByGid($gid);

		switch($gid) {
			case self::PROGRAM_MARKET_ENTER_GID:
				$program = new MarketEnterProgramForm($this->getProgramModel());
				break;
			case self::PROGRAM_LIMIT_POINT_GID:
				$program = new LimitPointProgramForm($this->getProgramModel());
				break;
		}

		if($program) {
			try {
				if(isset($_REQUEST['program_id'])) {
					if($entity->getId() == $_REQUEST['program_id']) {
						if($program->validateForm($entity)) {
							$program->saveForm($entity);
							header('Location: ' . SITE_PATH . 'index.php');
						}
					}
				} else {
					return $program->getForm($entity);
				}
			} catch(\Exception $e) {
				$_SESSION['error_message'] = base64_encode($e->getMessage());
				$_SESSION['error_program_id'] = $entity->getId();
			}
		}
	}

	/**
	 * @return ProgramModel
	 */
	public function getProgramModel(): ProgramModel
	{
		if(!$this->programModel) {
			$this->programModel = new ProgramModel($this->db);
		}

		return $this->programModel;
	}
}