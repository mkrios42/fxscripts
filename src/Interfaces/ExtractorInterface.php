<?php


namespace Fxscripts\Interfaces;

use Fxscripts\Entities\ExtractorRequest;
use Fxscripts\Entities\ExtractorResponse;

interface ExtractorInterface
{
    /**
     * @param ExtractorRequest $apiRequestParams
     * @return ExtractorResponse
     */
    public function getUrl(ExtractorRequest $apiRequestParams);
}