<?php


namespace Fxscripts\Interfaces;


use Fxscripts\Entities\RequestParamsEntity;

interface ProviderInterface
{
	public function getCurrentRate(RequestParamsEntity $requestParams);
	public function getPreviousRate(RequestParamsEntity $requestParams);
}