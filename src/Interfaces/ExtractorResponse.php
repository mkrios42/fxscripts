<?php
/**
 * Created by PhpStorm.
 * User: mkislitsyn
 * Date: 22.03.2019
 * Time: 12:18
 */

namespace Fxscripts\Entities;

class ExtractorResponse
{
	private $response;

	public function getResponseArray()
    {
        return json_decode($this->response, true);
    }

	public function getResponse()
	{
		return $this->response;
	}

    /**
     * @param $data
     * @return $this
     */
	public function setResponse($data)
	{
		$this->response = $data;
		return $this;
	}
}