<?php


namespace Fxscripts\Interfaces;

use Fxscripts\Entities\ProgramEntity;
use Fxscripts\Models\ProgramModel;

interface ProgramFormInterface
{
	public function __construct(ProgramModel $model);
	public function getForm(ProgramEntity $programEntity);
	public function validateForm(ProgramEntity $programEntity);
	public function saveForm(ProgramEntity $programEntity);
}