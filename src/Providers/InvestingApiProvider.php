<?php


namespace Fxscripts\Providers;


use Fxscripts\Entities\RateEntity;
use Fxscripts\Entities\RequestParamsEntity;
use Fxscripts\Entities\ExtractorRequest;
use Fxscripts\Extractors\GuzzleExtractor;
use Fxscripts\Interfaces\ProviderInterface;

class InvestingApiProvider implements ProviderInterface
{
	private $baseUrl = 'https://tvc4.forexpros.com';

	private $symbols = [
		RequestParamsEntity::PAIR_GBPUSD => [
			'symbol' => 2,
			'symbolEncoded' => ':GBP/USD'
		]
	];

	/**
	 * @param RequestParamsEntity $requestParams
	 *
	 * @return bool|RateEntity
	 * @throws \Exception
	 */
	public function getPreviousRate(RequestParamsEntity $requestParams)
	{
		$extractorSettingsEntity = new ExtractorRequest();
		$extractorSettingsEntity->setBaseUri($this->baseUrl);
		$extractorSettingsEntity->setEndpoint($this->createEndpoint() . '/7/7/18/history');
		$extractorSettingsEntity->setHttpMethod('GET');

		$params = [
			'symbol' => $this->getSymbol($requestParams->getPair()),
			'from' => $requestParams->getDateFrom()->getTimestamp(),
			'to' => $requestParams->getDateTo()->getTimestamp(),
			'resolution' => $requestParams->getDuration()
		];

		$extractorSettingsEntity->setRequestParams($params);
		$extractor = new GuzzleExtractor();

		$response = $extractor->getUrl($extractorSettingsEntity);

		if($data = $response->getResponseArray()) {
			$rateEntity = new RateEntity();
			$rateEntity->setPair($requestParams->getPair());
			$rateEntity->setOpenPrice($data['o'][0]);
			$rateEntity->setClosePrice($data['c'][0]);
			$rateEntity->setLowPrice($data['l'][0]);
			$rateEntity->setHighPrice($data['h'][0]);
			$rateEntity->setDateOpenPrice($requestParams->getDateFrom()->format(DATETIME_FORMAT));
			$rateEntity->setDateClosePrice($requestParams->getDateTo()->format(DATETIME_FORMAT));

			return $rateEntity;
		}

		return false;
	}

	public function getCurrentRate(RequestParamsEntity $requestParams)
	{
		$extractorSettingsEntity = new ExtractorRequest();
		$extractorSettingsEntity->setBaseUri($this->baseUrl);
		$extractorSettingsEntity->setEndpoint($this->createEndpoint() . '/1/1/8/quotes');
		$extractorSettingsEntity->setHttpMethod('GET');

		$params = [
			'symbols' => $this->getSymbolEncoded($requestParams->getPair()),
		];

		$extractorSettingsEntity->setRequestParams($params);
		$extractor = new GuzzleExtractor();

		$response = $extractor->getUrl($extractorSettingsEntity);
		if($data = $response->getResponseArray()) {
			$rateData = $data['d'][0]['v'];
			$rateEntity = new RateEntity();
			$rateEntity->setPair($requestParams->getPair());
			$rateEntity->setCurrentPrice($rateData['lp']);
			$rateEntity->setAskPrice($rateData['ask']);
			$rateEntity->setBidPrice($rateData['bid']);
			$rateEntity->setOpenPrice($rateData['open_price']);
			$rateEntity->setHighPrice($rateData['high_price']);
			$rateEntity->setLowPrice($rateData['low_price']);
			$rateEntity->setClosePrice($rateData['prev_close_price']);
			$rateEntity->setDatePrice($requestParams->getDateFrom()->format(DATETIME_FORMAT));

			return $rateEntity;
		}

		return false;
	}

	private function createEndpoint()
	{
		$hash = 'a8cecbf794b6267080c2b43b13a38fb2';
		$timestamp = (new \DateTime())->getTimestamp();
		return "/{$hash}/{$timestamp}";
	}

	private function getSymbol($pair)
	{
		return isset($this->symbols[$pair]) ? $this->symbols[$pair]['symbol'] : null;
	}

	private function getSymbolEncoded($pair)
	{
		return isset($this->symbols[$pair]) ? $this->symbols[$pair]['symbolEncoded'] : null;
	}
}