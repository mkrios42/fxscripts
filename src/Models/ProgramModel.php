<?php


namespace Fxscripts\Models;


use Fxscripts\Entities\ProgramEntity;

class ProgramModel
{
	private $table = 'fx_programs';

	/**
	 * @var \DB
	 */
	private $db;

	public function __construct(\DB $db)
	{
		$this->db = $db;
	}

	public function getAllPrograms()
	{
		$results = [];

		$sql = "SELECT * FROM `{$this->table}`";
		$rows = $this->db->query($sql);
		foreach($rows as $row) {
			$entity = $this->createEntity($row);
			$results[] = $entity;
		}

		return $results;
	}

	/**
	 * @param $id
	 *
	 * @return ProgramEntity
	 */
	public function getProgramById($id)
	{
		$result = null;

		$sql = "SELECT * FROM `{$this->table}` WHERE id=:id";
		$row = $this->db->row($sql, ["id" => $id]);
		if(!empty($row)) {
			$result = $this->createEntity($row);
		}

		return $result;
	}

	/**
	 * @param $id
	 *
	 * @return ProgramEntity
	 */
	public function getProgramByGid($gid)
	{
		$result = null;

		$sql = "SELECT * FROM `{$this->table}` WHERE gid=:gid";
		$row = $this->db->row($sql, ["gid" => $gid]);
		if(!empty($row)) {
			$result = $this->createEntity($row);
		}

		return $result;
	}

	public function updateProgram(ProgramEntity $entity)
	{
		$sql = "UPDATE {$this->table} SET 
            `status` = '{$entity->getStatus()}',
            `settings` = '{$entity->getSettings()}'
            WHERE `id` = {$entity->getId()}
            ";

		$res = $this->db->query($sql);
		if(!$res) {
			echo "false";
		}
	}

	/**
	 * @param $row
	 *
	 * @return ProgramEntity
	 */
	private function createEntity($row)
	{
		$entity = new ProgramEntity();
		$entity->setId($row['id']);
		$entity->setGid($row['gid']);
		$entity->setName($row['name']);
		$entity->setStatus($row['status']);
		$entity->setSettings($row['settings']);

		return $entity;
	}
}