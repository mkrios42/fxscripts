<?php


namespace Fxscripts\ProgramForms;


use Fxscripts\Entities\ProgramEntity;
use Fxscripts\Interfaces\ProgramFormInterface;
use Fxscripts\Models\ProgramModel;

class LimitPointProgramForm implements ProgramFormInterface
{
	/**
	 * @var ProgramModel
	 */
	private $model;

	/**
	 * LimitPointProgram constructor.
	 *
	 * @param ProgramModel $model
	 */
	public function __construct(ProgramModel $model)
	{
		$this->model = $model;
	}

	public function getForm(ProgramEntity $programEntity)
	{
		$settings = $programEntity->getSettingsArray();
		$timePoint = isset($settings['time_point']) ? $settings['time_point'] : null;
		$ratePoint = isset($settings['rate_point']) ? $settings['rate_point'] : null;
		$date = new \DateTime();

		if($programEntity->getStatus()) {
			$actionName = 'Выключить';
			$status = 0;
		} else {
			$actionName = 'Включить';
			$status = 1;
		}

		$actionLink = SITE_PATH . "index.php";

		ob_start();
		?>
		<div class="program_block">
			<div class="program_block--title">
				Название программы:
				<strong>
					<?php echo $programEntity->getName(); ?>
				</strong>
			</div>
			<div class="program_block--actions">
				<form action="<?=$actionLink?>" method="POST">
					<input type="hidden" name="program_id" value="<?=$programEntity->getId()?>">
					<input type="hidden" name="status" value="<?=$status?>">
					Время: <input type="text" name="time_point" value="<?=$timePoint?>" placeholder="<?=$date->format('Y-m-d H:i:s')?>"><br>
					Цена: <input type="text" name="rate_point" value="<?=$ratePoint?>"><br>
					Действие: <input type="submit" name="change_status" value="<?=$actionName?>"><br>
				</form>
			</div>
		</div>
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	/**
	 * @param ProgramEntity $programEntity
	 *
	 * @return bool
	 * @throws \Exception
	 */
	public function validateForm(ProgramEntity $programEntity)
	{
		$settings = [];

		if(isset($_REQUEST['status'])) {
			$programStatus = boolval($_REQUEST['status']);

			if($programStatus) {
				if(isset($_REQUEST['time_point']) && $_REQUEST['time_point']) {
					try {
					    $date = new \DateTime($_REQUEST['time_point']);
						$settings['time_point'] = $date->format(DATETIME_FORMAT);
					} catch(\Exception $e) {
						throw new \Exception('Некорректная дата');
					}
				} else {
					throw new \Exception('Пустое поле даты');
				}

				if(isset($_REQUEST['rate_point']) && $_REQUEST['rate_point']) {
					$settings['rate_point'] = $_REQUEST['rate_point'];
				} else {
					throw new \Exception('Пустое поле цены');
				}

				if(!empty($settings)) {
					$programEntity->setSettingsArray($settings);
				}
            }

			$programEntity->setStatus($programStatus);

			return true;
		}

		return false;
	}

	public function saveForm(ProgramEntity $programEntity)
	{
		$this->model->updateProgram($programEntity);
	}
}