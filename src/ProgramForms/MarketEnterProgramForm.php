<?php


namespace Fxscripts\ProgramForms;

use Fxscripts\Entities\ProgramEntity;
use Fxscripts\Interfaces\ProgramFormInterface;
use Fxscripts\Models\ProgramModel;

class MarketEnterProgramForm extends ProgramForm implements ProgramFormInterface
{
	/**
	 * @var ProgramModel
	 */
	private $model;

	public function __construct(ProgramModel $model)
	{
		$this->model = $model;
	}

	public function getForm(ProgramEntity $programEntity)
	{
	    $content = null;

		if($programEntity->getStatus()) {
			$actionName = 'Выключить';
			$actionLink = SITE_PATH . "index.php?program_id={$programEntity->getId()}&status=0";
		} else {
			$actionName = 'Включить';
			$actionLink = SITE_PATH . "index.php?program_id={$programEntity->getId()}&status=1";
		}

		ob_start();
		?>
		<div class="program_block">
			<div class="program_block--title">
				Название программы:
				<strong>
					<?php echo $programEntity->getName(); ?>
				</strong>
			</div>
			<div class="program_block--actions">
				Действие: <a href="<?=$actionLink?>"><?=$actionName?></a>
			</div>
		</div><br><br><br>
		<?php
		$content = ob_get_contents();
		ob_end_clean();

		return $content;
	}

	public function validateForm(ProgramEntity $programEntity)
	{
	    if(isset($_REQUEST['status'])) {
		    $programStatus = boolval($_REQUEST['status']);
		    $programEntity->setStatus($programStatus);
		    return true;
        }

		return false;
	}

	public function saveForm(ProgramEntity $programEntity)
	{
		$this->model->updateProgram($programEntity);
	}
}