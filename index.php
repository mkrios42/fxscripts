<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require("settings.php");

use Fxscripts\Controllers\FxscriptsFormController;

$controller = new FxscriptsFormController($db);
$controller->getProgramForms();