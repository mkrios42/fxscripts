<?php

session_start();

require_once __DIR__ . '/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::create(__DIR__, 'settings.env');
$dotenv->load();

$db = new Db(
    getenv('DBHost'),
    getenv('DBPort'),
    getenv('DBName'),
    getenv('DBUser'),
    getenv('DBPassword')
);

define("SITE_PATH", getenv('SITE_PATH'));
define("DATETIME_FORMAT", 'Y-m-d H:i:s');
define("PHANTHOM_JS_PATH", getenv('PHANTHOM_JS_PATH'));
define("SITE_MAIL", getenv('SITE_MAIL'));

date_default_timezone_set('Europe/Moscow');